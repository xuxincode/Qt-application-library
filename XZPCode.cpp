﻿#include "XZPCode.h"
#include "ui_XZPCode.h"
#include"kits/Tools.h"
#include"DemoUI/DemoWidget.h"
#include"exception/XException.h"
#include"hidusb/HidDev.h"
#include"ssh/SshAdapter.h"

#ifdef PIN_YIN
#include"pinyin/PinyinAdapter.h"
#endif

XZPCode::XZPCode(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::XZPCode)
{
    ui->setupUi(this);

}

XZPCode::~XZPCode()
{
    xPrint;

    delete ui;
}

void XZPCode::testException()
{
    QStringList mList;
    try{
        if(mList.isEmpty()) throw XException("QStringList is Empty");
        mList.takeFirst();

    }catch(XException& e){
        xPrint<<e.what();
    }
}


void XZPCode::testPinyin()
{

#ifdef PIN_YIN
    PinyinAdapter pyAdapter;
    QString pinyin; //拼音输出
    QString abbrev; //首字母简写
    pyAdapter.pinyinParse(tr("从前有座山"),pinyin,abbrev);
    xPrint<<pinyin<<abbrev;
#elif
    xPrint<<"please load pinyin module first ";
#endif

}

void XZPCode::on_btn_exception_clicked()
{
    testException();
}

void XZPCode::on_btn_pinyin_clicked()
{
    testPinyin();
}

void XZPCode::on_btn_Div_widget_clicked()
{
    DemoWidget::Instance()->show();
}

void XZPCode::on_btn_crash_clicked()
{
    QStringList m;
    m.takeFirst();

}
