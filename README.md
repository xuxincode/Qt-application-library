# Qt application library 

#### 介绍
包含Qt应用开发的常用接口库的使用方法

    camera:摄像头应用，开关摄像头，截图

    exception:简单异常使用

    hidusb:usb通信，包括枚举usb设备，读写数据

    mqtt:mqtt通信，包括订阅和发布

    pinyin:中文转拼音，以及缩写
    
    qrcode：二维码生成
    
    sql:数据库的简单使用
    
    ssh:ssh通信，比如登录linux操作命令    
    
    uart:串口通信
    
    xApp:对QApplication的封装，包含dump文件的生成，日志的处理
    
    player:占位
    
    recode:占位

#### 使用说明

    需要什么模块就在pro文件里面配置就可以了，比如camera模块
    在项目工程配置文件pro中添加include(camera/camera.pri)

    

