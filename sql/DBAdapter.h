﻿#ifndef DBADAPTER_H
#define DBADAPTER_H

#include<QSqlDatabase>
#include<QApplication>
#include<QSqlQuery>
#include<QSqlError>
#include"kits/Config.h"

/*
@projectName DBAdapter.h
@brief       数据库接口的封装
@author      xzp
@date        2019-06-04
*/
// sqlite语法 ：https://www.runoob.com/sqlite/sqlite-tutorial.html

namespace XZPSQL {

    static  QSqlDatabase _db;
    /*
    @brief 打印支持的数据库类型
    */
    static void printDrivers()const
    {
        xPrint<<QSqlDatabase::drivers();
    }
    /*
    @brief 打开数据库
    @param(type)    数据库类型
    @return         false失败 true成功
    */
    bool openSql(const QString& db,const QString& type)
    {
        if(db.isEmpty()) return false;
        if(_db.isOpen()) return true;

        _db = QSqlDatabase::addDatabase(type);
        _db.setDatabaseName(db);
        if(!_db.open()){
            xErr<<_db.lastError().text();
            return false;
        }
        return true;
    }
    /*
    @brief         关闭数据库
    @return
    */
    void closeSql()
    {
        _db.close();
    }
    /*
    @brief 获取所有表名
    @return        返回表名列表
    */
    QStringList getTables() const
    {
        return _db.tables();
    }

    /*
    @brief 创建表格
    @param(tableName)   表名
    @param(sqlCmd)      创建表格指令
    @return             false,true
    */
    //such as sqlCmd = "create table loginInfo("username varchar(20),password varchar(20)")
    bool createTable(const QString& tableName,const QString& sqlCmd)
    {
        if(_db.tables().contains(tableName)) return;
        QSqlQuery query(_db);
        try{
            query.prepare(sqlCmd);
            query.exec();
        }catch(QSqlError &e){
            xErr<<e.text();
            return false;
        }
        return true;
    }
    /*
    @brief                  插入表格数据
    @param(tablename)       表名
    @param(datas)           要插入表格的数据列表
    @return                 true,false
    */
    /*
    @brief 执行sql命令
    @param(sqlCmd)       sql命令
    @return              true,false
    */
    bool runSqlCmd(const QString& sqlCmd)
    {
        QSqlQuery query;
        try{
           query.exec(sqlCmd);
        }catch(QSqlError& e){
           xErr<<e.text();
           return false;
        }
        return true;
    }
    /*
    @brief 查询数据
    @param(sqlCmd)       sql指令
    @param(keyIndex)     字段索引
    @return              查询结构列表
    */
    QVariantList queryTable(const QString& sqlCmd,const int keyIndex=0)
    {
        QVariantList vList;
        QSqlQuery query(sqlCmd);
        while (query.next()) {
            vList.append(query.value(keyIndex));
        }
        return vList;
    }
}

#endif // DBADAPTER_H
