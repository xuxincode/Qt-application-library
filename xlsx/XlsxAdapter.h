﻿#ifndef XLSXADAPTER_H
#define XLSXADAPTER_H
#include<QtXlsx>
#include"xlsxdocument.h"
#include <QObject>
#include"kits/PinyinAdapter.h"
#include"xlsxworksheet.h"
#include"xlsxworkbook.h"
#include<QList>


class XlsxAdapter : public QObject
{
    Q_OBJECT
public:
    explicit XlsxAdapter(QObject *parent = 0);
    ~XlsxAdapter();
    void setSuffix(QString suffix);
    void setSavePath(QString savePath);
signals:
    void progessValue(int value);
    void errString(QString errStr);
    void packageOver();


public slots:
    void onInitAccess(QString filename,int tableIndex,int customerId);
    void onRunTask();

private:
   QString productJson(QString songInfo);
   QString getSongInfo();
   void addBrackets();
   void winMoveFile(QString oldfile,QString newfile);
   bool openWriteFile();
   QString getNewName();
private:
    int     mAccIndex;      //表格读取索引记录位置
    int     mColsStartPos;  //表格栏目起始位置
    int     mColsEndPos;    //表格栏目结束位置
    int     mRowsEndPos;    //表格行数
    QString mSavePath;      //保存路径
    QString mSuffix;
    bool    mAccessIsInit;
    bool    mIsRun;

    QXlsx::Document *mXlDoc = nullptr;
    QXlsx::Worksheet *mWorkSheet=nullptr;

    PinyinAdapter mPinYingAdapter;
    QString       mContent;
    QFile           file;
    QStringList     mSuffixList;
    int         mWriteCount;
    int         mCustomerId;
    QList<QString> mDataList;


};

#endif // XLSXADAPTER_H
