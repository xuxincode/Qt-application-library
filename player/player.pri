QT += multimedia

FORMS += \
    $$PWD/PlayerWidget.ui

HEADERS += \
    $$PWD/PlayerWidget.h \
    $$PWD/QPlayerAdapter.h \
    $$PWD/VlcPlayerAdapter.h

SOURCES += \
    $$PWD/PlayerWidget.cpp \
    $$PWD/QPlayerAdapter.cpp \
    $$PWD/VlcPlayerAdapter.cpp
