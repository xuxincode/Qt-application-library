#-------------------------------------------------
#
# Project created by QtCreator 2019-05-14T14:30:34
#
#-------------------------------------------------

QT       += core gui network



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = XZPCode
TEMPLATE = app
CONFIG   += warn_off  #关闭编译警告
CONFIG   += PIN_YIN   #自定义配置项

DEFINES += PIN_YIN    #自定义宏

include(xApp/xApp.pri)
include(kits/kits.pri)
include(DemoUI/DemoUI.pri)
include(exception/exception.pri)
include(sql/sql.pri)
include(uart/uart.pri)
include(hidusb/hidusb.pri)
include(ssh/ssh.pri)
include(mqtt/mqtt.pri)
include(camera/camera.pri)
include(qrcode/qrcode.pri)
#include(player/player.pri)



CONFIG(PIN_YIN){        #表示如果有PIN_YIN宏定义就加载
    include(pinyin/pinyin.pri)

    win32: LIBS += -L$$PWD/pinyin/ -lpinyin4cpp

    INCLUDEPATH += $$PWD/pinyin
    DEPENDPATH += $$PWD/pinyin

}


SOURCES += main.cpp\
        XZPCode.cpp

HEADERS  += XZPCode.h

FORMS    += XZPCode.ui

