﻿#ifndef CONFIG_H
#define CONFIG_H
#include<QDebug>
#include<QApplication>

#define ROOTPATH    qApp->applicationDirPath()
#define xPrint      qInfo()<<__FUNCTION__<<"["<<__LINE__<<"]"
#define xErr        qCritical()<<"Error:"<<__FUNCTION__<<"["<<__LINE__<<"]"
#define LOG_FILE    ROOTPATH + "/logger.txt"
#define CONFIG_FILE ROOTPATH + "/config.ini"

#define MQTT_ADDRESS             "addr"
#define MQTT_TOPIC               "topic"
#define MQTT_USERNAME            "username"
#define MQTT_PASSWORD            "password"
#define MQTT_QOS                  0
#define MQTT_TIMEOUT              10000L


#endif // CONFIG_H
