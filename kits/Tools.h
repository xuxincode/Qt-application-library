﻿#ifndef TOOLS_H
#define TOOLS_H
#include"Config.h"
#include<QSettings>

#pragma execution_character_set("utf-8")

static QVariant nConfigValue(const QString& key,const QString& group=QString("config"))
{
    QSettings setting(CONFIG_FILE,QSettings::IniFormat);
    setting.beginGroup(group);
    QVariant value = setting.value(key);
    setting.endGroup();
    return value;
}

static void nWriteConfig(const QString& key,const QString& value,const QString& group=QString("config"))
{
    QSettings setting(CONFIG_FILE,QSettings::IniFormat);
    setting.beginGroup(group);
    setting.setValue(key,value);
    setting.endGroup();
}

static bool deBugOpen()
{
   QVariant debug = nConfigValue("debug");
   if(debug.isNull() || !debug.toInt())
       return false;
   return true;
}

static bool multOpen()
{
    QVariant mult = nConfigValue("mult");
    if(mult.isNull() || !mult.toInt())
        return false;
    return true;
}

#endif // TOOLS_H
