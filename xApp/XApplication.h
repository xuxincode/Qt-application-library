#ifndef XAPPLICATION_H
#define XAPPLICATION_H

#include <QObject>
#include<QApplication>


class XApplication : public QApplication
{
    Q_OBJECT
public:
    explicit XApplication(const char* appName,int& argc, char **argv);
    ~XApplication();
private:
    void checkOnly(const char* appName);
};

#endif // XAPPLICATION_H
