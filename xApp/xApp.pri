HEADERS += \
    $$PWD/XApplication.h

SOURCES += \
    $$PWD/XApplication.cpp


CONFIG += debug_and_release warn_off
CONFIG += thread exceptions rtti stl

#生成pdb文件
QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
#QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO
QMAKE_LFLAGS_RELEASE = /INCREMENTAL:NO /DEBUG

LIBS += -L$$PWD/crash/ -lqBreakpad
INCLUDEPATH += $$PWD/crash
DEPENDPATH += $$PWD/crash
