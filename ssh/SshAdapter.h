﻿#ifndef SSHADAPTER_H
#define SSHADAPTER_H

#include <QObject>
#include"QSshManager.h"

class SshAdapter:public QObject
{
    Q_OBJECT
public:
    SshAdapter(QObject* parent=0);
    void login(const QString& ipAddr,const QString& username,const QString& password);
    void runCmd(const QString& cmd);
    void cmdResult(const char* data);
    void loginResult(bool result);
    QSshManager* getSshManger()const;
private:
    QSshManager *mSshManger;
    int         channelNo=-1;    //表示渠道的序号
};

#endif // SSHADAPTER_H
