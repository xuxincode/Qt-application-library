QT += concurrent

HEADERS += \
    $$PWD/SshAdapter.h \
    $$PWD/Channel.h \
    $$PWD/QSshManager.h \
    $$PWD/Ssh2.h

SOURCES += \
    $$PWD/SshAdapter.cpp \
    $$PWD/Channel.cpp \
    $$PWD/QSshManager.cpp \
    $$PWD/Ssh2.cpp

win32: LIBS += -L$$PWD/libssh2_lib/ -llibssh2

INCLUDEPATH += $$PWD/libssh2_lib/include
DEPENDPATH += $$PWD/libssh2_lib/
