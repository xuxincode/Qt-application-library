﻿#ifndef CAMERAADAPTER_H
#define CAMERAADAPTER_H

#include <QWidget>
#include<QCamera>
#include<QCameraViewfinder>
#include<QCameraImageCapture>

namespace Ui {
class CameraAdapter;
}

class CameraAdapter : public QWidget
{
    Q_OBJECT

public:
    explicit CameraAdapter(QWidget *parent = 0);
    ~CameraAdapter();
    void closeCamera()const;
    void openCamera(const QString& description=QString());
    void enumCamera();
private slots:
    void onImageCaptured(int id,QImage img);
    void onImageSaved(int id, const QString &fileName);

private:
    Ui::CameraAdapter *ui;
    QCamera*               mCamera;         //摄像头对象
    QCameraViewfinder*     mViewfinder;     //摄像头取景器
    QCameraImageCapture*   mImageCapture;   //截图对象

};

#endif // CAMERAADAPTER_H
