#ifndef CAMRARWIDGETONE_H
#define CAMRARWIDGETONE_H

#include <QWidget>

namespace Ui {
class CamrarWidgetOne;
}

class CamrarWidgetOne : public QWidget
{
    Q_OBJECT

public:
    explicit CamrarWidgetOne(QWidget *parent = 0);
    ~CamrarWidgetOne();

private:
    Ui::CamrarWidgetOne *ui;
};

#endif // CAMRARWIDGETONE_H
