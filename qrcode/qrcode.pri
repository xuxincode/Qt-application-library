
HEADERS += \
    $$PWD/QrcodeAdapter.h \
    $$PWD/qrcode/bitstream.h \
    $$PWD/qrcode/config.h \
    $$PWD/qrcode/mask.h \
    $$PWD/qrcode/mmask.h \
    $$PWD/qrcode/mqrspec.h \
    $$PWD/qrcode/qrencode.h \
    $$PWD/qrcode/qrencode_inner.h \
    $$PWD/qrcode/qrinput.h \
    $$PWD/qrcode/qrspec.h \
    $$PWD/qrcode/rscode.h \
    $$PWD/qrcode/split.h

SOURCES += \
    $$PWD/qrcode/bitstream.c \
    $$PWD/qrcode/mask.c \
    $$PWD/qrcode/mmask.c \
    $$PWD/qrcode/mqrspec.c \
    $$PWD/qrcode/qrencode.c \
    $$PWD/qrcode/qrinput.c \
    $$PWD/qrcode/qrspec.c \
    $$PWD/qrcode/rscode.c \
    $$PWD/qrcode/split.c
