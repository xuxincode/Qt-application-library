#ifndef XEXCEPTION_H
#define XEXCEPTION_H
#include<QException>


class XException:public QException
{
public:
    XException(QString errStr);
    void raise() const override;
    XException* clone()const override;
    const char* what() const override;
private:
    QString   errString;
};

#endif // XEXCEPTION_H
