#include "XException.h"

XException::XException(QString errStr):errString(errStr)
{

}

void XException::raise() const
{
    throw *this;
}

XException *XException::clone() const
{
    return new XException(*this);
}

const char *XException::what() const
{
    return errString.toLocal8Bit().data();
}
