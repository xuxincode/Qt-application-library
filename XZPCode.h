﻿#ifndef XZPCODE_H
#define XZPCODE_H

#include <QWidget>

namespace Ui {
class XZPCode;
}

class XZPCode : public QWidget
{
    Q_OBJECT

public:
    explicit XZPCode(QWidget *parent = 0);
    ~XZPCode();
    void testException();
    void testPinyin();
private slots:


    void on_btn_exception_clicked();

    void on_btn_pinyin_clicked();

    void on_btn_Div_widget_clicked();

    void on_btn_crash_clicked();

private:
    Ui::XZPCode *ui;
};

#endif // XZPCODE_H
