﻿#ifndef UARTDEV_H
#define UARTDEV_H

#include <QObject>
#include<QSerialPort>

class UartDev : public QObject
{
    Q_OBJECT
public:
    explicit UartDev(QObject *parent = 0);
    bool openCom(const QString com);
    void closeCom() const;
    void printComInfos();
    void sendCmd(const QString& cmd);
signals:

private slots:
    void onReadData();
private:
    QSerialPort*    mSerialPort;
};

#endif // UARTDEV_H
