﻿#include "UartDev.h"
#include<QSerialPortInfo>
#include"kits/Config.h"

UartDev::UartDev(QObject *parent) : QObject(parent)
{
    mSerialPort = new QSerialPort(this);
    connect(mSerialPort,SIGNAL(readyRead()),this,SLOT(onReadData()));
}

bool UartDev::openCom(const QString com)
{
    if(mSerialPort->isOpen()) return true;
    mSerialPort->setPortName(com);  //设置串口名称
    if(mSerialPort->open(QIODevice::ReadWrite)) //打开串口
    {
        mSerialPort->setBaudRate(QSerialPort::Baud115200); //设置比特率
        mSerialPort->setDataBits(QSerialPort::Data8);      //设置数据位
        mSerialPort->setParity(QSerialPort::NoParity);     //设置校验位
        mSerialPort->setStopBits(QSerialPort::OneStop);    //设置停止位
        mSerialPort->setFlowControl(QSerialPort::NoFlowControl);   //设置流控制
        return true;

    }
    return false;
}

void UartDev::closeCom() const
{
    if(mSerialPort->isOpen())
        mSerialPort->close();
}

void UartDev::printComInfos()
{
   for(auto com:QSerialPortInfo::availablePorts())
   {
       xPrint<<com.portName()<<com.description();
   }
}

void UartDev::sendCmd(const QString &cmd)
{
    if(!mSerialPort->isOpen()) return;
    mSerialPort->write(cmd.toLatin1()); //QByteArray::fromHex(cmd.toLatin1())
}

void UartDev::onReadData()
{
    QByteArray dataArray = mSerialPort->readAll();
    xPrint<<dataArray;
}
