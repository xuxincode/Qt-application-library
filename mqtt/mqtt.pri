HEADERS += \
    $$PWD/MqttAdapter.h

SOURCES += \
    $$PWD/MqttAdapter.cpp

win32: LIBS += -L$$PWD/mqtt/lib/ -lpaho-mqtt3c
win32: LIBS += -L$$PWD/mqtt/lib/ -lpaho-mqtt3a

INCLUDEPATH +=$$PWD/mqtt/include
DEPENDPATH +=$$PWD/mqtt/lib
