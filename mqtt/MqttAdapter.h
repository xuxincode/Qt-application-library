﻿#ifndef MQTTADAPTER_H
#define MQTTADAPTER_H

#include <QObject>
#include"MQTTClient.h"

class MqttAdapter:public QObject
{
    Q_OBJECT
public:
    MqttAdapter(QObject* parent=0);
    ~MqttAdapter();
    bool initMqtt(const QString& clientId);
    void subScribe(const QString& toptic);
    int publicMessage(const QString& message,const QString& topic);
    void recvMessage(const char *data,char* topic);
    void connectLost(char* cause);
private:
    void printConnectError(const int errCode)const;
private:
    MQTTClient  mClient;
    bool        mhasConn = false;
};

#endif // MQTTADAPTER_H
