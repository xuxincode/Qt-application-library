﻿#ifndef PINYINADAPTER_H
#define PINYINADAPTER_H

#include <QObject>

class PinyinAdapter : public QObject
{
    Q_OBJECT
public:
    explicit PinyinAdapter(QObject *parent = 0);
    void pinyinParse(QString &content,QString& pingying,QString& abbrev);


};

#endif // PINYINADAPTER_H
