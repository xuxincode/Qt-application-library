﻿#include "PinyinAdapter.h"
#include "PinyinHelper.h"
#include<QList>
#include<exception>


PinyinAdapter::PinyinAdapter(QObject *parent) : QObject(parent)
{

}

void PinyinAdapter::pinyinParse(QString &content, QString &pingying, QString &abbrev)
{

    if(content.isEmpty()) return;
    HanyuPinyinOutputFormat outputFormat;
    QList<QString> pinyinList;
    QChar ch;

    for(int m = 0; m < content.length();++m)
    {
        pinyinList.clear();
        ch= content.at(m);
        if(ch.isSpace()){
            pingying.append(" ");
          continue;
        }
        PinyinHelper::toHanyuPinyinStringArray(ch,&outputFormat,&pinyinList);

        if(!pinyinList.isEmpty()){
            QString result = pinyinList.at(0);
            pingying.append(result).append(" ");
            abbrev.append(result.at(0));
        }
        else pingying.append(ch);
    }
    pingying.replace("-"," ");
    abbrev.replace("-"," ");
}
