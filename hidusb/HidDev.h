﻿#ifndef HIDDEV_H
#define HIDDEV_H

#include <QThread>
#include"hidapi.h"
class HidDev : public QObject
{
    Q_OBJECT
public:
    explicit HidDev(QObject *parent = 0);
    ~HidDev();
    void enumHidDev();
    bool openHidDev(unsigned short vid, unsigned short pid);
    void closeHidDev();
    int hidWrite(const unsigned char *data, size_t length);
    int hidRead();
private:
    hid_device* mHidDev = nullptr;
    bool mRun = true;
};

#endif // HIDDEV_H
