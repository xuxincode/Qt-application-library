﻿#include "HidDev.h"
#include"kits/Config.h"
#define HidDataLength 255


HidDev::HidDev(QObject *parent) : QObject(parent)
{
    hid_init();
}

HidDev::~HidDev()
{
    mRun = false;

}

void HidDev::enumHidDev()
{
    hid_device_info *root = hid_enumerate(0,0);
    while (root) {
        xPrint<<QString::fromWCharArray(root->product_string) <<root->vendor_id<<root->product_id;
        root = root->next;
    }
}

bool HidDev::openHidDev(unsigned short vid, unsigned short pid)
{
    if(!mHidDev) return true;
    mHidDev = hid_open(vid,pid,NULL);
    return mHidDev !=NULL;
}

void HidDev::closeHidDev()
{
    hid_close(mHidDev);
    hid_exit();
    mHidDev = nullptr;
}

int HidDev::hidWrite(const unsigned char *data, size_t length)
{
    return hid_write(mHidDev,data,length);
}

int HidDev::hidRead()
{
    unsigned char recData[HidDataLength] = {0};
    int res = hid_read(mHidDev,recData, HidDataLength);
    xPrint<<recData;
    return res;
}
