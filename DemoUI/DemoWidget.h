﻿#ifndef DEMOWIDGET_H
#define DEMOWIDGET_H

#include<QDialog>

#include<QScopedPointer>

namespace Ui {
class DemoWidget;
}

class DemoWidget : public QDialog
{
    Q_OBJECT

public:
    static DemoWidget *Instance();

    ~DemoWidget();
    void initUI();
    void showSystemIco();
protected:
    bool event(QEvent *event);


private:
    explicit DemoWidget(QWidget *parent = 0);
    Q_DISABLE_COPY(DemoWidget) //禁止拷贝构造和赋值构造
private:
    Ui::DemoWidget *ui;
    static QScopedPointer<DemoWidget> self;
};


#define qDemoWidget DemoWidget::Instance()
#endif // DEMOWIDGET_H
