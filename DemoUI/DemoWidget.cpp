﻿#include "DemoWidget.h"
#include "ui_DemoWidget.h"
#include<QMouseEvent>
#include<QMutex>
#include<QMutexLocker>
#include<QDesktopServices>
#include<QToolButton>
#include"kits/Tools.h"

QScopedPointer<DemoWidget> DemoWidget::self;
DemoWidget *DemoWidget::Instance()
{
    if(self.isNull()){
        QMutex mutex;
        QMutexLocker locker(&mutex);
        if(self.isNull()){
            self.reset(new DemoWidget);
        }
    }
    return self.data();
}

DemoWidget::DemoWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DemoWidget)
{
    ui->setupUi(this);
    xPrint;
    //设置透明背景
//    setAttribute(Qt::WA_TranslucentBackground);
    //设置无边框窗口,窗口置顶
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    //设置右侧底部可拖动改变窗口大小 QDialog有此属性，QWidget没有此属性
    setSizeGripEnabled(true);
    //系统默认工具打开文件
//    QDesktopServices::openUrl(QUrl(fileName));

    initUI();

}

DemoWidget::~DemoWidget()
{
    xPrint;
    delete ui;
}

void DemoWidget::initUI()
{
   //用Qt自带图标设置按钮图标
   ui->tBtn_SysIcon->setIcon(style()->standardIcon(QStyle::SP_ComputerIcon));
   ui->tBtn_Close->setIcon(style()->standardIcon(QStyle::SP_TitleBarCloseButton));

   QStringList qss;
   qss.append("QLabel{color:#ffffff;}");
   qss.append("#tBtn_Close,#tBtn_SysIcon{border:none;border-radius:0px;}");
   qss.append("#tBtn_Close:hover{background-color:#ff0000;}");
   qss.append("#tBtn_Close{border-top-right-radius:5px;}");
   qss.append("#labTitle{font:bold 16px;}");
   qss.append("#labStatus{font:15px;}");
   this->setStyleSheet(qss.join(""));

   connect(ui->tBtn_Close,&QToolButton::clicked,[=](bool pressed){Q_UNUSED(pressed) this->close();});
   showSystemIco();
}

//设置窗口可拖动
bool DemoWidget::event(QEvent *event)
{
    static QPoint mousePoint;
    static bool mousePressed = false;

    if(event->type() == QEvent::MouseButtonPress
            || event->type() == QEvent::MouseButtonPress
            || event->type() == QEvent::MouseMove){

        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);

        if(mouseEvent->type() == QMouseEvent::MouseButtonPress){
            if(mouseEvent->button() == Qt::LeftButton){
                mousePressed = true;
                mousePoint = mouseEvent->globalPos() - this->pos();
            }
        }else if(mouseEvent->type() == QMouseEvent::MouseButtonPress){
            mousePressed = false;
        }else if(mouseEvent->type() == QMouseEvent::MouseMove){
           if(mousePressed && (mouseEvent->buttons() && Qt::LeftButton)){
               this->move(mouseEvent->globalPos() - mousePoint);
           }
        }
    }

    return QWidget::event(event);
}


void DemoWidget::showSystemIco()
{

    int lastIndex = int(QStyle::SP_LineEditClearButton);
    for(int _index = 0;_index<= lastIndex;++_index){
        QToolButton *btn = new QToolButton(this);
        btn->resize(50,30);
        btn->move(60+ 60*(_index%10) ,80 + 30*(_index/10));
        btn->show();
        btn->setIcon(style()->standardIcon((QStyle::StandardPixmap)_index));
    }

}

